package app1;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.PathVariable;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.*;

import com.fasterxml.jackson.annotation.JsonView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
	

@RestController
@RequestMapping("/api/v1")
public class ControllerClass{
	HashMap<Integer,Moderator> Moderator_HashMap = new HashMap<Integer,Moderator>();
	HashMap<String,Poll> Poll_HashMap = new HashMap<String,Poll>();
	static int moderator_id_counter=123456;
	static int poll_id_counter=60096;
	String username="foo";
	String password="bar";
	String authString = username + ":" + password;
	byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
	String authStringEnc = new String(authEncBytes);
	
	/*
	 * public Object createModerator(
	 * @RequestBody @Valid Moderator moderator,
	 * @RequestHeader(value = "Authorization") String authorizationDetail,
	 * HttpServletResponse httpResponse)
	 */

	//VALID CHECK
	//Create moderator
	@RequestMapping (value="/moderators", method=RequestMethod.POST, headers={"content-type=application/json"})
	public @ResponseBody Moderator create_Moderator(@Valid @RequestBody Moderator moderator,HttpServletResponse httpResponse)
	/*@RequestHeader(value = "Authorization") String Input_authString)*/
	{
		moderator_id_counter++;
		moderator.setId(moderator_id_counter);
		//moderator.setCreated_at(new Date());
		
		//Date date = calendar.getTime();
		Date date=new Date();
        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);
		moderator.setCreated_at(formatted.substring(0, 22) + ":" + formatted.substring(22));
		moderator.setCreated_polls(new ArrayList<Integer>());
		Moderator_HashMap.put(moderator_id_counter,moderator);
		httpResponse.setStatus(HttpServletResponse.SC_CREATED);//201
		/*
		 * Calendar calendar;
        Date date = calendar.getTime();
        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);
        return formatted.substring(0, 22) + ":" + formatted.substring(22);
		 */
		
		/*String[] SplitInput_authString=Input_authString.split(" ");
		if(SplitInput_authString[1].equals(authStringEnc))
		{
			Moderator_HashMap.put(moderator_id_counter,moderator);
			httpResponse.setStatus(201);
		}*/
		return moderator;
	}
	
	//Get moderator
	@RequestMapping (value="/moderators/{moderator_id}", method=RequestMethod.GET, headers={"accept=application/json"})
	public @ResponseBody Moderator get_Moderator(@PathVariable int moderator_id, @RequestHeader(value = "Authorization") String Input_authString,HttpServletResponse httpResponse)
	{
		Moderator moderator=null;
		
		moderator=new Moderator();
		String[] SplitInput_authString=Input_authString.split(" ");
		/*if(Input_authString.isEmpty())
		{
			httpResponse.setStatus(HttpServletResponse.SC_PROXY_AUTHENTICATION_REQUIRED);//407
		}
		else */if(SplitInput_authString[1].equals(authStringEnc))
		{
			moderator = Moderator_HashMap.get(moderator_id);
			httpResponse.setStatus(HttpServletResponse.SC_OK);//200
		}
		else
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);//401
			//HttpServletResponse.
			//httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}
		return moderator;
	}
	
	//VALID CHECK
	//Update moderator
	@RequestMapping (value="/moderators/{moderator_id}", method=RequestMethod.PUT, headers={"content-type=application/json"})
	public @ResponseBody Moderator update_Moderator(@PathVariable int moderator_id, /*@Valid*/ @RequestBody Moderator moderator, @RequestHeader(value = "Authorization") String Input_authString, HttpServletResponse httpResponse)
	{
		Moderator updated_moderator=new Moderator();
		String[] SplitInput_authString=Input_authString.split(" ");
		if(SplitInput_authString[1].equals(authStringEnc))
		{
			updated_moderator = Moderator_HashMap.get(moderator_id);
			updated_moderator.setName(moderator.getName());
			updated_moderator.setEmail(moderator.getEmail());
			updated_moderator.setPassword(moderator.getPassword());
			httpResponse.setStatus(HttpServletResponse.SC_CREATED);//201
		}
		else
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);//401
		}
		return updated_moderator;
	}
	
/*	//VALID CHECK
	//Create poll
	@RequestMapping (value="/moderators/{moderator_id}/polls", method=RequestMethod.POST, headers={"content-type=application/json"})
	@ResponseBody
	public Poll create_Poll(@PathVariable int moderator_id,@Valid @RequestBody Poll poll, @RequestHeader(value = "Authorization") String Input_authString,HttpServletResponse httpResponse)
	{
		String[] SplitInput_authString=Input_authString.split(" ");
		if(SplitInput_authString[1].equals(authStringEnc))
		{
			poll_id_counter++;
			Moderator_HashMap.get(moderator_id).getCreated_polls().add(poll_id_counter); 
			//Moderator_HashMap.get(moderator_id).getHm_polls().put(poll.getpollid(), poll);
			poll.setpollid(Integer.toString(poll_id_counter,36));
			poll.setResults(new ArrayList<Integer>());
			int choice_count=poll.getChoice().size();
			for(int i=1; i<=choice_count; ++i){
				poll.getResults().add(0);
			}
			Poll_HashMap.put(Integer.toString(poll_id_counter,36), poll);
			httpResponse.setStatus(HttpServletResponse.SC_CREATED);//201
		}
		else
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);//401
		}
		return poll;
	}
*/

	//VALID CHECK
	//Create poll
	@RequestMapping (value="/moderators/{moderator_id}/polls", method=RequestMethod.POST, headers={"content-type=application/json"})
	@ResponseBody
	public Poll create_Poll(@PathVariable int moderator_id,@Valid @RequestBody Poll poll, @RequestHeader(value = "Authorization") String Input_authString,HttpServletResponse httpResponse)
	{
		String[] SplitInput_authString=Input_authString.split(" ");
		if(SplitInput_authString[1].equals(authStringEnc))
		{
			poll_id_counter++;
			//Moderator_HashMap.get(moderator_id).getCreated_polls().add(poll_id_counter); 
			Moderator m=Moderator_HashMap.get(moderator_id);
			if(m!=null)
			{
				m.getCreated_polls().add(poll_id_counter); 
				poll.setid(Integer.toString(poll_id_counter,36));
				poll.setResults(new ArrayList<Integer>());
				int choice_count=poll.getChoice().size();
				for(int i=1; i<=choice_count; ++i){
					poll.getResults().add(0);
				}
				Poll_HashMap.put(Integer.toString(poll_id_counter,36), poll);
			}
			else
			{
				poll=null;
			}
			httpResponse.setStatus(HttpServletResponse.SC_CREATED);//201
		}
		else
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);//401
		}
		return poll;
	}
	
	//Get poll without Result
	@JsonView(View.PollWithoutResult.class)
	@RequestMapping (value="/polls/{poll_id_36}", method=RequestMethod.GET, headers={"accept=application/json"})
	public @ResponseBody Poll get_Poll_without_result(@PathVariable String poll_id_36)
	{
		Poll poll=Poll_HashMap.get(poll_id_36);
		if(poll==null)
		{
			poll=new Poll();
		}
		return poll;
	}
	
	public boolean find_poll_in_moderator(int moderator_id,String poll_id_36)
	{
		int poll_id=(int) Long.parseLong(poll_id_36, 36);
		boolean has_flag=Moderator_HashMap.get(moderator_id).getCreated_polls().contains(poll_id);
		if(has_flag==true)
			return true;
		else
			return false;
	}
	
	//Get poll with Result
	@JsonView(View.PollWithResult.class)
	@RequestMapping (value="/moderators/{moderator_id}/polls/{poll_id_36}", method=RequestMethod.GET, headers={"accept=application/json"})
	public @ResponseBody Poll get_Poll_with_result(@PathVariable int moderator_id, @PathVariable String poll_id_36,@RequestHeader(value = "Authorization") String Input_authString, HttpServletResponse httpResponse)
	{
		Poll poll=new Poll();
		String[] SplitInput_authString=Input_authString.split(" ");
		if(SplitInput_authString[1].equals(authStringEnc))
		{
			if(find_poll_in_moderator(moderator_id,poll_id_36)==true){
				poll=Poll_HashMap.get(poll_id_36);
			}
		}
		else
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);//401
		}
		return poll;
	}
	
	//Delete a Poll
	@RequestMapping (value="/moderators/{moderator_id}/polls/{poll_id_36}", method=RequestMethod.DELETE)
	public @ResponseBody void delete_poll(@PathVariable int moderator_id,@PathVariable String poll_id_36, @RequestHeader(value = "Authorization") String Input_authString,HttpServletResponse httpResponse)
	{
		String[] SplitInput_authString=Input_authString.split(" ");
		if(SplitInput_authString[1].equals(authStringEnc))
		{
			if(find_poll_in_moderator(moderator_id,poll_id_36)==true){
				Moderator_HashMap.get(moderator_id).getCreated_polls().remove(Long.parseLong(poll_id_36, 36));
				Poll_HashMap.remove(poll_id_36);
				httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);//204
			}
		}
		else
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);//401
		}	
	}
	
	//List all polls with result
	@JsonView(View.PollWithResult.class)
	@RequestMapping (value="/moderators/{moderator_id}/polls", method=RequestMethod.GET, headers="accept=application/json")
	public @ResponseBody ArrayList<Poll> List_polls(@PathVariable int moderator_id, @RequestHeader(value = "Authorization") String Input_authString,HttpServletResponse httpResponse)
	{
		/*
		 * try{
		 * }catch (Exception e) {
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
		 */
		ArrayList<Poll> moderator_polls=new ArrayList<Poll>();
		String[] SplitInput_authString=Input_authString.split(" ");
		if(SplitInput_authString[1].equals(authStringEnc))
		{
			Moderator m=Moderator_HashMap.get(moderator_id);
			ArrayList<Integer> moderator_poll_ids=m.getCreated_polls();
			for(int i=0; i<moderator_poll_ids.size(); ++i)
			{
				int poll_id_10=moderator_poll_ids.get(i);
				String poll_id_36=Integer.toString(poll_id_10,36);
				moderator_polls.add(Poll_HashMap.get(poll_id_36));
			}
			httpResponse.setStatus(HttpServletResponse.SC_OK);//200
		}
		
		else
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);//401
		}
		return moderator_polls;
	}
	
	//VALID CHECK
	//Vote for a Poll
	@RequestMapping (value="/polls/{poll_id_36}/choice/{choice_index}", method=RequestMethod.PUT)
	public @ResponseBody void vote_a_poll(@PathVariable String poll_id_36, @PathVariable int choice_index)
	{
		Poll poll=Poll_HashMap.get(poll_id_36);
		ArrayList<Integer> update_results=poll.getResults();
		update_results.set(choice_index, poll.getResults().get(choice_index)+1);
		poll.setResults(update_results);
		
		//poll.setResults(poll.getResults().set(choice_index, poll.getResults().get(choice_index)+1));
		
	}
}
